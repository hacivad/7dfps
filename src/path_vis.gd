extends Node
#this only runs in editor, to help with visualizing paths
# ^ couldn't get it to work
var path_nodes
var pathvec = []

export(bool) var enable_path_vis = false

func _ready():
	path_nodes = self.get_children()

	for i in range(path_nodes.size()):
		pathvec.push_back(path_nodes[i].transform.origin)
		path_nodes[i].get_child(0).hide()


func _process(delta):
	if enable_path_vis:
		for i in range(pathvec.size()):
			DrawLine3D.DrawLine(pathvec[i], pathvec[(i + 1) % pathvec.size()], Color(1, 1, 1), delta)
			#print("line drawn from ", i, " to ", (i + 1) % pathvec.size())
