extends Area

var speed = 1
var dir = Vector3(0, 1, 0)
var t = Vector3(0, 0, 0)

func fireball_init(s: int, target: Vector3):
	speed = s
	t = target

func _ready():
	dir = (t - self.transform.origin).normalized()
	
func _physics_process(delta):
	translate(dir * speed * delta)


func _on_firbals_body_entered(body):
	if body.has_method("hit"):
		body.hit(10)
		
	queue_free()
