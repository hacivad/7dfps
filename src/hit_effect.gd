extends Spatial

onready var dust = $Particles
var lifetime = 1

# Called when the node enters the scene tree for the first time.
func _ready():
	#print("particles ready")
	dust.set_emitting(true)
#	dust.set_amount(8)
#	dust.set_explosiveness_ratio(1.0)
#	dust.set_one_shot(true)
#	dust.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if lifetime <= 0:
		queue_free()
		
	lifetime -= delta
