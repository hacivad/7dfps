extends Button

func _on_Options_mouse_entered():
	SoundManager.play_hover()

func _on_Options_pressed():
	SoundManager.play_click()
	if self.get_child(0).is_visible():
		self.get_child(0).hide()
	else:
		self.get_child(0).show()
