extends Button

func _on_Fullscreen_mouse_entered():
	SoundManager.play_hover()

func _on_Fullscreen_pressed():
	SoundManager.play_click()
	OS.window_fullscreen = !OS.window_fullscreen
