extends Node

var curr_level = 1
var arena_kill_count = 0

# Dictionary of level scene paths.
var levelDict = {
	0 : '',
	1 : 'res://levels/Level1.tscn',
	2 : 'res://levels/Level2.tscn',
	3 : 'res://levels/Level3.tscn',
	4 : 'res://levels/Level4.tscn',
	5 : 'res://levels/Level5.tscn',
	6 : 'res://levels/Level6.tscn',
	7 : 'res://levels/Level7.tscn',
	8 : 'res://levels/Level8.tscn',
	9 : 'res://levels/Level9.tscn',
	10 : 'res://levels/Level10.tscn',
	11 : 'res://interface/EndScreen.tscn'
}
