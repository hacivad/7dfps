extends KinematicBody

onready var navmesh = get_node("../enemy_navmesh")
onready var player = get_node("../Player")

var path = null
var path_index = 1

var speed = 5
var min_distance = 10
#in seconds
var nav_update_delay = 2.5
var nav_update_countdown = 0

func move(delta):
	#figure out how far to move and in what direction
	#using move and slide because it makes things easier
	var dir = Vector3(0, 0, 0)		
	if path != null and !path.empty():
		dir = path[path_index] - self.transform.origin
		dir = dir.normalized() * speed * delta
	
		var remainder = (path[path_index] - (self.transform.origin + dir))
		var dest_dir = (path[path_index] - self.transform.origin).normalized()

		var overshot = dest_dir.dot(remainder.normalized())
		if (overshot < 0):
			#we've gone too far along the path to the current node, and need to turn to the next node
			#so move the rest of the distance to to the current node before switching
			translate(path[path_index] - self.transform.origin)

			#if there are nodes in the path left
			if (path_index < path.size() - 1):
				path_index += 1
				dir = (path[path_index] - self.transform.origin).normalized()
				dir *= remainder.length()		
	
	translate(dir)

func _imp_ai():
	pass

func _process(delta):
	if nav_update_countdown <= 0:
	#if Input.is_action_just_pressed("ui_cancel"):
		#print("getting path")
		nav_update_countdown = nav_update_delay
		path = navmesh.get_simple_path(self.transform.origin, player.transform.origin)
		path_index = 1
		
	nav_update_countdown -= delta
	
	#if Input.is_action_pressed("ui_focus_next"):
	move(delta)
		
