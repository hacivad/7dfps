extends Node

#thought the player node should be in here too
#player stats
#these get modified by cards
#use these wherever you need them
#add more for cards if you need them
var speed = 15
var sens = 20.0
var gravity = -9.8
var jump_speed = 5
var can_jump = 1

#health stats
var max_health = 50
var cur_health = max_health
var cur_health_per = 1.0

func set_health(hp: float):
	max_health = hp
	cur_health = hp * cur_health_per
	
func dmg_player(dmg: float):
	cur_health -= dmg
	cur_health_per = cur_health / max_health

#gun base stats
var firedmg = 10.0
var firerange = 40.0
var firerate = 4.0
var firedelay = 1 / firerate

func set_firerate(frate):
	firerate = frate
	firedelay = 1 / firerate

func reset_to_base():
	#movement
	speed = 15.0
	gravity = -9.8
	jump_speed = 5
	
	#gun
	firedmg = 10.0
	firerange = 40
	set_firerate(4.0)
	
	#health
	set_health(50)
		
#mag swapping stuff
var cur_mag = CardManager.card_container_1

func get_mag_from_num(num):
	if num == 3:
		return CardManager.card_container_3
	elif num == 2:
		return CardManager.card_container_2
	else:
		#if the input is wrong, which it shouldn't be, we'll just default to mag 1
		return CardManager.card_container_1
	
