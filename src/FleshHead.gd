extends MeshInstance

onready var rotate = $Rotate

func _ready():
	rotate.play('rotate')

func _on_Rotate_animation_finished(anim_name):
	rotate.play('rotate')
