extends TextureRect

var co_tex = preload('res://assets/textures/card2.stex')
var ra_tex = preload('res://assets/textures/card3.stex')
var un_tex = preload('res://assets/textures/card1.stex')

export(int) var card_ID

# Initializing card description.
func _ready():
	$CardDesc.text = CardManager.descDict[card_ID]
	if card_ID in CardManager.rarity_table[0]:
		self.texture = co_tex
	if card_ID in CardManager.rarity_table[1]:
		self.texture = ra_tex
	if card_ID in CardManager.rarity_table[2]:
		self.texture = un_tex

# Will open and close the card menu.
func _on_Card_gui_input(event):
	if event is InputEventMouseButton:
		if event.pressed and event.button_index == 1:
			SoundManager.play_click()
			if $CardMenu.is_visible():
				$CardMenu.hide()
			else:
				$CardMenu.show()

# Will send a copy of the card to a new container and delete this instance.
# TODO - Would probably be best to write a 'cardCopy' function for these.
func _on_Button_Mag1_pressed():
	if CardManager.mag_1.get_child(0).get_children().size() < 10:
		SoundManager.play_load()
		var cardCopy = CardManager.cardBaseScene.instance()
		cardCopy.card_ID = self.card_ID
		CardManager.mag_1.get_child(0).add_child(cardCopy)
		queue_free()
	else:
		SoundManager.play_invalid()

func _on_Button_Mag2_pressed():
	if CardManager.mag_2.get_child(0).get_children().size() < 10:
		SoundManager.play_load()
		var cardCopy = CardManager.cardBaseScene.instance()
		cardCopy.card_ID = self.card_ID
		CardManager.mag_2.get_child(0).add_child(cardCopy)
		queue_free()
	else:
		SoundManager.play_invalid()

func _on_Button_Mag3_pressed():
	if CardManager.mag_3.get_child(0).get_children().size() < 10:
		SoundManager.play_load()
		var cardCopy = CardManager.cardBaseScene.instance()
		cardCopy.card_ID = self.card_ID
		CardManager.mag_3.get_child(0).add_child(cardCopy)
		queue_free()
	else:
		SoundManager.play_invalid()

func _on_Button_Ret_pressed():
	SoundManager.play_unload()
	var cardCopy = CardManager.cardBaseScene.instance()
	cardCopy.card_ID = self.card_ID
	CardManager.coll.get_child(0).add_child(cardCopy)
	queue_free()

# Just for flair.
func _on_Card_mouse_entered():
	SoundManager.play_hover()
	self.rect_size.x += 4
	self.rect_size.y += 4
	self.rect_position.x -= 2
	self.rect_position.y += 2

func _on_Card_mouse_exited():
	self.rect_size.x -= 4
	self.rect_size.y -= 4
	self.rect_position.x += 2
	self.rect_position.y -= 2
