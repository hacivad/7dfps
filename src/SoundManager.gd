extends Node

# Call these functions to play sound effects.
# To add new sounds, first add them to 'Sound.tscn'.
# Using 'get_child()' here is a lazy solution. But it works.

func play_load():
	Sound.get_child(0).play()
	
func play_unload():
	Sound.get_child(1).play()
	
func play_click():
	Sound.get_child(2).play()
	
func play_hover():
	Sound.get_child(3).play()
	
func play_ambient1():
	Sound.get_child(4).play()
	
func stop_ambient1():
	Sound.get_child(4).stop()

func play_portal():
	Sound.get_child(5).play()
	
func play_shoot():
	Sound.get_child(6).play()
	
func play_level():
	Sound.get_child(7).play()
	
func stop_level():
	Sound.get_child(7).stop()
	
func play_jump():
	Sound.get_child(8).play()
	
func play_footsteps():
	Sound.get_child(9).play()
	
func stop_footsteps():
	Sound.get_child(9).stop()
	
func play_fireball():
	Sound.get_child(10).play()
	
func play_hurt():
	Sound.get_child(11).play()
	
func play_invalid():
	Sound.get_child(12).play()

func play_enemy_death():
	Sound.get_child(13).play()
	
func play_spawn():
	Sound.get_child(14).play()
