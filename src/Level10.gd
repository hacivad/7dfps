extends Spatial

onready var imp_spawn = preload('res://assets/Imp2.tscn')
onready var exit_spawn = preload('res://assets/LevelExit2.tscn')

onready var spawn_nodes = $SpawnNodes
onready var wave_timer = $WaveTimer
onready var lone_path = $path_0

var wave_count = 1
var spawned_exit = false

func _ready():
	spawn_wave()
	wave_timer.start()

func spawn_wave():
	var spawn_nodes_vec = spawn_nodes.get_children()
	for i in range(spawn_nodes_vec.size()):
		var new_imp = imp_spawn.instance()
		new_imp.transform.origin = spawn_nodes_vec[i].transform.origin
		new_imp.parent_nodepath = lone_path
		self.add_child(new_imp)
	SoundManager.play_spawn()

func _on_WaveTimer_timeout():
	if wave_count < 5:
		spawn_wave()
		wave_count += 1
		wave_timer.start()
	
func _process(_delta):
	if WorldManager.arena_kill_count >= 50 and spawned_exit == false:
		spawned_exit = true
		var new_exit = exit_spawn.instance()
		new_exit.transform.origin = lone_path.get_child(0).transform.origin
		self.add_child(new_exit)
		SoundManager.play_spawn()
